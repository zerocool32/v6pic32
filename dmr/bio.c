/*
 * bio.c
 * Common block device routines.
 */

#include "../h/param.h"
#include "../h/user.h"
#include "../h/buf.h"
#include "../h/conf.h"
#include "../h/systm.h"
#include "../h/proc.h"
#include "../h/types.h"

struct buf* incore(dev_t, blk_t);
struct buf buf[NBUF];
struct buf swbuf;
char buffers [NBUF][512];
struct buf bfreelist;
blk_t rablock;
int nblkdev;

/*
 * The following several routines allocate and free
 * buffers with various side effects.  In general the
 * arguments to an allocate routine are a device and
 * a block number, and the value is a pointer to
 * to the buffer header; the buffer is marked "busy"
 * so that no on else can touch it.  If the block was
 * already in core, no I/O need be done; if it is
 * already busy, the process waits until it becomes free.
 * The following routines allocate a buffer:
 *	getblk
 *	bread
 *	breada
 * Eventually the buffer must be released, possibly with the
 * side effect of writing it out, by using one of
 *	bwrite
 *	bdwrite
 *	bawrite
 *	brelse
 */

/* 
 * List of functions present in bio.c, in order:
 * 
 * getblk
 * bread
 * breada
 * bwrite
 * bdwrite
 * bawrite
 * brelse
 * incore
 * notavail
 * iodone
 * clrbuf
 * binit
 * bflush
 * geterror
 * swap
 * physio
 */

/*
 * Assign a buffer for the given block.  If the appropriate
 * block is already associated, return it; otherwise search
 * for the oldest non-busy buffer and reassign it.
 * When a 512-byte area is wanted for some random reason
 * (e.g. during exec, for the user arglist) getblk can be called
 * with device NODEV to avoid unwanted associativity.
 */
struct buf*
getblk(dev_t dev, blk_t blkno)
{
	register struct buf *bp;
	register struct devtab *dp;
	word save;

	if(major(dev) >= nblkdev)
		panic("blkdev");

    loop:
	if (dev < 0)
		dp = &bfreelist;
	else {
		dp = bdevsw[major(dev)].d_tab;
		if(dp == NULL)
			panic("devtab");
		for (bp=dp->b_forw; bp != dp; bp = bp->b_forw) {
			if (bp->b_blkno!=blkno || bp->b_dev!=dev)
				continue;
			save = lock1();
			if (bp->b_flags&B_BUSY) {
				bp->b_flags |= B_WANTED;
				sleep(bp, PRIBIO);
				goto loop;
			}
			unlock(save);
			notavail(bp);

			return(bp);
		}
	}
	save = lock1();
	if (bfreelist.av_forw == &bfreelist) {
		bfreelist.b_flags |= B_WANTED;
		sleep(&bfreelist, PRIBIO);
		unlock(save);
		goto loop;
	}
	unlock(save);
	notavail(bp = bfreelist.av_forw);
	if (bp->b_flags & B_DELWRI) {
		bp->b_flags |= B_ASYNC;
		bwrite(bp);
		goto loop;
	}
	bp->b_flags = B_BUSY;
	bp->b_back->b_forw = bp->b_forw;
	bp->b_forw->b_back = bp->b_back;
	bp->b_forw = dp->b_forw;
	bp->b_back = dp;
	dp->b_forw->b_back = bp;
	dp->b_forw = bp;
	bp->b_dev = dev;
	bp->b_blkno = blkno;

	return(bp);
}

/*
 * Read in (if necessary) the block and return a buffer pointer.
 */
struct buf*
bread(dev_t dev, blk_t blkno)
{
	register struct buf *rbp;

	rbp = getblk(dev, blkno);
	if (rbp->b_flags&B_DONE)
		return(rbp);
	rbp->b_flags |= B_READ;
	rbp->b_wcount = -256;
	(*bdevsw[major(dev)].d_strategy)(rbp);
	iowait(rbp);
	return(rbp);
}

/*
 * Read in the block, like bread, but also start I/O on the
 * read-ahead block (which is not allocated to the caller)
 */
struct buf*
breada(dev_t dev, blk_t blkno, blk_t rablkno)
{
	register struct buf *rbp, *rabp;

	rbp = NULL;
	if (!incore(dev, blkno)) {
		rbp = getblk(dev, blkno);
		if ((rbp->b_flags&B_DONE) == 0) {
			rbp->b_flags |= B_READ;
			rbp->b_wcount = -256;
			(*bdevsw[major(dev)].d_strategy)(rbp);
		}
	}
	if (rablkno && !incore(dev, rablkno)
	     && (bfreelist.av_forw != &bfreelist)) {
		rabp = getblk(dev, rablkno);
		if (rabp->b_flags & B_DONE)
			brelse(rabp);
		else {
			rabp->b_flags |= B_READ|B_ASYNC;
			rabp->b_wcount = -256;
			(*bdevsw[major(dev)].d_strategy)(rabp);
		}
	}
	if (rbp==0)
		return(bread(dev, blkno));
	iowait(rbp);
	return(rbp);
}

/*
 * Write the buffer, waiting for completion.
 * Then release the buffer.
 */
void
bwrite(register struct buf *rbp)
{
	register int flag;

	flag = rbp->b_flags;
	rbp->b_flags &= ~(B_READ | B_DONE | B_ERROR | B_DELWRI);
	rbp->b_wcount = -256;
	(*bdevsw[major(rbp->b_dev)].d_strategy)(rbp);
	if ((flag&B_ASYNC) == 0) {
		iowait(rbp);
		brelse(rbp);
	} else if ((flag&B_DELWRI)==0)
		geterror(rbp);
}

/*
 * Release the buffer, marking it so that if it is grabbed
 * for another purpose it will be written out before being
 * given up (e.g. when writing a partial block where it is
 * assumed that another write for the same block will soon follow).
 * This can't be done for magtape, since writes must be done
 * in the same order as requested.
 */
void
bdwrite(register struct buf *rbp)
{
	rbp->b_flags |= B_DELWRI | B_DONE;
	brelse(rbp);
}

/*
 * Release the buffer, start I/O on it, but don't wait for completion.
 */
void
bawrite(register struct buf *rbp)
{
	rbp->b_flags |= B_ASYNC;
	bwrite(rbp);
}

/*
 * release the buffer, with no I/O implied.
 */
void
brelse(register struct buf *rbp)
{
	register struct buf **backp;
	word save;

	if (rbp->b_flags&B_WANTED)
		wakeup(rbp);
	if (bfreelist.b_flags&B_WANTED) {
		bfreelist.b_flags &= ~B_WANTED;
		wakeup(&bfreelist);
	}
	if (rbp->b_flags&B_ERROR)
		rbp->b_dev = 0xff;

	backp = &bfreelist.av_back;
	save = lock1();
	rbp->b_flags&= ~(B_WANTED|B_BUSY|B_ASYNC);

	(*backp)->av_forw = rbp;
	rbp->av_back = *backp;
	*backp = rbp;
	rbp->av_forw = &bfreelist;

	unlock(save);
}

/*
 * See if the block is associated with some buffer
 * (mainly to avoid getting hung up on a wait in breada)
 */
struct buf*
incore(dev_t dev, blk_t blkno)
{
	register struct buf *bp;
	register struct devtab *dp;

	dp = bdevsw[major(dev)].d_tab;
	for (bp=dp->b_forw; bp != dp; bp = bp->b_forw)
		if (bp->b_blkno==blkno && bp->b_dev==dev)
			return(bp);
	return(0);
}

/*
 * Wait for I/O completion on the buffer; return errors
 * to the user.
 */
void
iowait(register struct buf *rbp)
{
	word save;

	save = lock1();
	while ((rbp->b_flags&B_DONE)==0)
		sleep(rbp, PRIBIO);
	unlock(save);
	geterror(rbp);
}

/*
 * Unlink a buffer from the available list and mark it busy.
 * (internal interface)
 */
void
notavail(register struct buf *rbp)
{
	word save;

	save = lock1();
	rbp->av_back->av_forw = rbp->av_forw;
	rbp->av_forw->av_back = rbp->av_back;
	rbp->b_flags |= B_BUSY;
	unlock(save);
}

/*
 * Mark I/O complete on a buffer, release it if I/O is asynchronous,
 * and wake up anyone waiting for it.
 */
void
iodone(register struct buf *rbp)
{
	rbp->b_flags |= B_DONE;
	if (rbp->b_flags&B_ASYNC)
		brelse(rbp);
	else {
		rbp->b_flags &= ~B_WANTED;
		wakeup(rbp);
	}
}

/*
 * Zero the core associated with a buffer.
 */
void
clrbuf(struct buf *bp)
{
	register int *p;
	register int c;

	p = bp->b_addr;
	c = 256;
	do
		*p++ = 0;
	while (--c);
}

/*
 * Initialize the buffer I/O system by freeing
 * all buffers and setting all device buffer lists to empty.
 */
void
binit()
{
	register struct buf *bp;
	register struct devtab *dp;
	register int i;
	struct bdevsw *bdp;

	bfreelist.b_forw = bfreelist.b_back =
	    bfreelist.av_forw = bfreelist.av_back = &bfreelist;
	for (i=0; i<NBUF; i++) {
		bp = &buf[i];
		bp->b_dev = -1;
		bp->b_addr = buffers[i];
		bp->b_back = &bfreelist;
		bp->b_forw = bfreelist.b_forw;
		bfreelist.b_forw->b_back = bp;
		bfreelist.b_forw = bp;
		bp->b_flags = B_BUSY;
		brelse(bp);
	}
	i = 0;
	for (bdp = bdevsw; bdp->d_open; bdp++) {
		dp = bdp->d_tab;
		if(dp) {
			dp->b_forw = dp;
			dp->b_back = dp;
		}
		i++;
	}
	nblkdev = i;
}

/*
 * make sure all write-behind blocks
 * on dev (or NODEV for all)
 * are flushed out.
 * (from umount and update)
 */
void
bflush(int dev)
{
	register struct buf *bp;
	word save;

loop:
	save = lock1();
	for (bp = bfreelist.av_forw; bp != &bfreelist; bp = bp->av_forw) {
		if (bp->b_flags&B_DELWRI && (dev == NODEV||dev==bp->b_dev)) {
			bp->b_flags |= B_ASYNC;
			notavail(bp);
			bwrite(bp);
			goto loop;
		}
	}
	unlock(save);
}

/*
 * Pick up the device's error number and pass it to the user;
 * if there is an error but the number is 0 set a generalized
 * code.  Actually the latter is always true because devices
 * don't yet return specific errors.
 */
void
geterror(register struct buf *bp)
{
	if (bp->b_flags&B_ERROR)
		if ((u.u_error = bp->b_error)==0)
			u.u_error = EIO;
}


/*
 * swap I/O
 */
void
swap(blk_t blkno, gaddr_t coreaddr, word count, int rdflg)
{
	register int *fp;
	word save;

	fp = &swbuf.b_flags;
	save = lock1();
	while (*fp&B_BUSY) {
		*fp |= B_WANTED;
		sleep(fp, PSWP);
	}
	*fp =  B_SWAP | B_BUSY | B_PHYS | rdflg;
	swbuf.b_dev = swapdev;
	swbuf.b_wcount=-((count+1)/2);
	swbuf.b_blkno = blkno;
	swbuf.b_addr=(char *)(coreaddr&0xffff);
	swbuf.b_xmem=(dword)(coreaddr&0xffff0000L);
	(*bdevsw[major(swapdev)].d_strategy)(&swbuf);
	lock();

	while((*fp&B_DONE)==0)
		sleep(fp, PSWP);	/* Altered from PSWP on suggestion on Lyons book */
	if (*fp&B_WANTED)
		wakeup(fp);
	*fp &= ~(B_SWAP|B_BUSY|B_WANTED);
	if(*fp&B_ERROR)
		panic("swap: input/output error");
	return;
}

/*
 * Raw I/O. The arguments are
 *	The strategy routine for the device
 *	A buffer, which will always be a special buffer
 *	  header owned exclusively by the device for this purpose
 *	The device number
 *	Read/write flag
 * Essentially all the work is computing physical addresses and
 * validating them.
 */
void
physio(int (*strat)(), register struct buf *bp, dev_t dev, int rw)
{
	register char *base;
	word save;

	base = u.u_base;
	if (((dword)(word)base+(dword)u.u_count) > cup[DATAS/8])
	{
		u.u_error=EFAULT;
		return;
	}
	save = lock1();
	while (bp->b_flags&B_BUSY)
	{
		bp->b_flags|=B_WANTED;
		sleep((int)bp,PRIBIO);
	}
	bp->b_flags=B_BUSY|B_PHYS|rw;
	bp->b_dev=dev;
	bp->b_addr=base;
	bp->b_xmem=((dword)cup[DATAS/8]<<16);
		    cup[DATAS/8];
	bp->b_blkno=(blk_t)(u.u_offset>>9);
	bp->b_wcount= -((u.u_count>>1)&0xffff);
	bp->b_error=0;
	cup->p_flag|=SLOCK;
	(*strat)(bp);
	lock();
	while((bp->b_flags&B_DONE)==0)
		sleep((int)bp,PRIBIO);
	cup->p_flag&=~SLOCK;
	if (bp->b_flags&B_WANTED)
		wakeup((int)bp);
	unlock(save);
	bp->b_flags&=~(B_BUSY|B_WANTED);
	u.u_count=(-bp->b_resid)<<1;
	geterror(bp);
}
