#ifndef TYPES_H
#define TYPES_H

typedef	int		dev_t;		/*	device type	*/
typedef unsigned int    blk_t;		/*	block number	*/
typedef unsigned int	ino_t;		/*	inode number	*/
typedef unsigned long   offs_t;		/*	file offset	*/
typedef unsigned int	pid_t;		/*	process id	*/
typedef int		uid_t;		/*	user id type	*/
typedef int		gid_t;		/*	group id type	*/
typedef char*		caddr_t;	/*	core address	*/
typedef unsigned long	gaddr_t;	/*	global address	*/
typedef unsigned long	time_t;		/*	realtime	*/
typedef unsigned char 	u_char;
typedef unsigned int	u_int;
typedef unsigned long	u_long;
typedef unsigned char 	byte;
typedef unsigned int  	word;
typedef unsigned long 	dword;

/* Derived from RetroBSD */

typedef u_int size_t;
typedef int ssize_t;
typedef long off_t;

#endif /* TYPES_H */