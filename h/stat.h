/*
 * stat.h
 * Copyright (c) 1992 Szigeti Szabolcs
 */

#ifndef STAT_H
#define STAT_H

struct stat 
{
    int     s_dev;
    int     s_inumber;
    int     s_flags;
    char    s_nlinks;
    char    s_uid;
    char    s_gid;
    char    s_size0;
    int     s_size1;
    int     s_addr[8];
    long    s_actime;
    long    s_modtime;
};

#define S_ALLOC 0100000
#define S_TYPE  0060000
#define S_PLAIN 0000000
#define S_DIREC 0040000
#define S_CHAR  0020000
#define S_BLOCK 0060000
#define S_LARGE 0010000
#define S_SUID  0004000
#define S_STEXT 0001000
#define S_ISUID         0004000 /* set user id on execution             */
#define S_ISGID         0002000 /* set group id on execution            */
#define S_ISVTX         0001000 /* save swapped text even after use     */
#define S_IREAD         0000400 /* read permission, owner               */
#define S_IWRITE        0000200 /* write permission, owner              */
#define S_IEXEC         0000100 /* execute/search permission, owner     */

#endif /* STAT_H */