/*
 *	This file defines the zombie structure definition
 *	which is a basis for ZOMBIE alteration to system
 *	see changes.h for details
 *	only used in sys1.c
 *
 *	LONG_TIME also applies to this file
 */
struct	pzomb
{
	char	pz_stat;
	char	pz_flag;
	char	pz_pri;		/* priority, negative is high */
	char	pz_sig;		/* signal number sent to this process */
	int	pz_ur0;		/* returned status */
	char	pz_cpu;		/* cpu usage for scheduling */
	char	pz_nice;	/* nice for scheduling */
	int	pz_ttyp;	/* controlling tty */
	int	pz_pid;		/* unique process id */
	int	pz_ppid;	/* process id of parent */
	long	pz_utime;					   /* fix000 */
	long	pz_stime;	/* times returned from process */  /* fix000 */
};
