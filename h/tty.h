/*
 *	tty.h for new Non-Bell tty driver
 *	modified from original version to provide
 *	local compatibility
 *	Peter Collinson August 77
 *	New internal standard Nov 1979
 *	Interrupt		Escape	(old ^G) - implemented as a synonym
 *	Quit			Control-shift-backslash
 *	Delete char		Rubout
 *	Delete line		^X (^U)
 *	Repeat line		^R
 *	Delete word		^W
 *	End of text		^D
 *	Escape char		^P (^A)
 *	Pause			^Z (restart on any char )
 */
#define NOUSERBRK	/* take out user break mode */
/*
 * A clist structure is the head
 * of a linked list queue of characters.
 * The characters are stored in 4-word
 * blocks containing a link and 6 characters.
 * The routines getc, putc and zapc (m45.s or m40.s)
 * manipulate these structures.
 */
struct	clist  {
	int	c_cc;		/* character count */
	char	*c_cf;		/* pointer to first block */
	char	*c_cl;		/* pointer to last block */
	};

/*
 * A tty structure is needed for
 * each UNIX character device that
 * is used for normal terminal IO.
 * The routines in tty.c handle the
 * common code associated with
 * these structures.
 * The definition and device dependent
 * code is in each driver. (kl.c dc.c dh.c)
 */
struct	tty  {
	struct clist	t_inq;		/* input list from device */
	struct clist	t_outq;		/* output list to device */
	int		t_dev;		/* device name */
	int		t_speeds;	/* output and input line speeds */
	char		t_erase;	/* the character delete */
	char		t_kill;		/* the line delete */
	int		t_flags;	/* modes, settable via spcl fcn call */
	char		t_nldelay;	/* delay for newline */
	char		t_crdelay;	/* delay for cr */
	char		t_htdelay;	/* delay for horizontal tabs */
	char		t_vtdelay;	/* delay for vertical motion */
	char		t_width;	/* maximum line length for folding */
	char		t_length;	/* maximum screen length for paging */
#ifndef NOUSERBRK
	int		t_breaktab[8];	/* break character map */
#endif
	int		*t_addr;	/* device addr (registers or start routine */
	int		t_state;	/* internal state, not visible */
	char		t_line;		/* line number on screen */
	char		t_col;		/* column number on line */
	char		t_delct;	/* number of delimiters in queue */
	char		t_char;		/* scratch byte for the driver */
	};


#define	TTIPRI	10
#define	TTOPRI	20

#define	CNEXTLINE	012	/* newline, print another line in paging */
#define	CERASE		0177	/* rubout, the default character delete */
#define	CINTR		033	/* ESCAPE, interrupt process */
#define	CEOT		004	/* ^D, end of file indicator */
#define CEOTALT		031	/* ^Y, alternative eof - EMAS can't change */
#define	CXOFF		032	/* ^Z, stop transmission */
#define	CXON		021	/* ^q, restart transmission - this altered to any char at UKC  */
#define	CXDELETE	017	/* ^o, delete output */
#define CBELL		07
#define CVTAB		013
#define CFORM		014
#define	CRETYPE		022	/* ^r, retype current line */
#define CSTATUS		024	/* ^t, report system status */
#define	CLITERAL	001	/* ^A, the literal escape char */
#define CLITALT		020	/* ^P Alternative literal char */
#define CDWORD		027	/* ^w, word delete character */
#define	CKILL		030	/* ^x, the default line delete char */
#define	CQUIT		034	/* ^\, quit */
#define	CDELIM		0377	/* internal delimiter, also used in zapc (m40.s) */

/* limits */
#define	TTHIWAT	50
#define	TTLOWAT	20
#define	TTYHOG	132

/* modes */
#define	HUPCL		01
#define	XTABS		02
#define	LCASE		04
#define	ECHO		010
#define	CRMOD		020
#define	RAW		040
#define	ODDP		0100
#define	EVENP		0200
#define	SCOPE		0400
#define INDCTL		01000
#define	USERBREAK	02000
#define	ALL8BITS	04000
#define	HOLD		010000

/* delay fields */
#define	NLDELAY		03
#define TBDELAY		014
#define CRDELAY		060
#define VTDELAY		0100

/* Internal state bits */
#define	TIMEOUT		01	/* Delay timeout in progress */
#define	WOPEN		02	/* Waiting for open to complete */
#define	ISOPEN		04	/* Device is open */
#define	SSTART		010	/* Has special start routine at addr */
#define	CARR_ON		020	/* Software copy of carrier-present */
#define	BUSY		040	/* Output in progress */
#define	ASLEEP		0100	/* Wakeup when output done */
#define LITERAL		0200	/* last char was a literal escape */
#define	XHOLD		0400	/* output is being held up */
#define	XSTOP		01000	/* stop write from filling the outq */
#define	XDELETE		02000	/* delete output */
#define ERASING		04000	/* erase string in progress */

/* ERASING brackets */
#define CERASEOPEN	'['
#define CERASECLOSE	']'
