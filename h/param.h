/*
 * param.h
 * System parameters
 */

#ifndef PARAM_H
#define PARAM_H

/* Changing any of these variables
 * requires a kernel rebuild
 */

#define CPU		mips	/* processor type */

#define	NBUF	20		/* size of buffer cache */
#define	NINODE	100		/* number of in core inodes */
#define	NFILE	100		/* number of in core file structures */
#define	NMOUNT	5		/* number of mountable file systems */
#define	NOFILE	15		/* max open files per process */
#define	CANBSIZ	80		/* max size of typewriter line */
#define	CMAPSIZ	100		/* size of core allocation area */
#define	SMAPSIZ	100		/* size of swap allocation area */
#define	NCALL	16		/* max simultaneous time callouts */
#define	NPROC	50		/* max number of processes */
#define	NTEXT	40		/* max number of pure texts */
#define	NCLIST	100		/* max total clist size */
#define	HZ		50		/* frequency of system clock */

/*
 * priorities
 * probably should not be
 * altered too much
 */

#define	PSWP	-100
#define	PINOD	-90
#define	PRIBIO	-50
#define	PPIPE	1
#define	PWAIT	40
#define	PSLEP	90
#define	PUSER	100

/*
 * signals
 * don't change, only add
 */

#define		NSIG	20
#define		SIGHUP	1	/* hangup */
#define		SIGINT	2	/* interrupt */
#define		SIGQIT	3	/* quit */
#define		SIGINS	4	/* illegal instruction */
#define		SIGTRC	5	/* trace or breakpoint */
#define		SIGIOT	6	/* iot */
#define		SIGABRT	7	/* abort */
#define		SIGFPT	8	/* floating exception */
#define		SIGKIL	9	/* kill */
#define		SIGBUS	10	/* bus error */
#define		SIGSEG	11	/* segmentation fault */
#define		SIGSYS	12	/* bad syscall */
#define		SIGPIPE	13	/* broken pipe */
#define		SIGBND	14	/* bound exceeded */
#define		SIGTERM 15	/* terminate */
#define		SIGOVF	16	/* overflow trap */

/*
 * fundamental constants
 * cannot be changed
 */

#define	NULL	0
#define	NODEV	(-1)
#define	ROOTINO	1		/* i number of all roots */
#define	DIRSIZ	14		/* max characters per directory */

#define	minor(x)	((x)&0xff)	/* minor device */
#define major(x)	((x)>>8)	/* major device */
#define makedev(ma,mi)	(((ma)<<8)+(mi)&0xff)

#define min(a,b)	((a)<(b)?(a):(b))
#define max(a,b)	((a)<(b)?(b):(a))

extern  long sys_x (int,...);

#endif /* PARAM_H */