/*
 * conf.h
 * Definition of the block and character device switch tables
 */

#ifndef CONF_H
#define CONF_H

/*
 * Declaration of block device
 * switch. Each entry (row) is
 * the only link between the
 * main unix code and the driver.
 * The initialization of the
 * device switches is in the
 * file conf.c.
 */
struct	bdevsw
{
	int	(*d_open)();
	int	(*d_close)();
	int	(*d_strategy)();
	int	*d_tab;
};

/*
 * Nblkdev is the number of entries
 * (rows) in the block switch. It is
 * set in binit/bio.c by making
 * a pass over the switch.
 * Used in bounds checking on major
 * device numbers.
 */
int	nblkdev;

/*
 * Character device switch.
 */
struct	cdevsw
{
	int	(*d_open)();
	int	(*d_close)();
	int	(*d_read)();
	int	(*d_write)();
	int	(*d_sgtty)();
};

/*
 * Number of character switch entries.
 * Set by cinit/tty.c
 */
int	nchrdev;

extern struct bdevsw bdevsw[];
extern struct cdevsw cdevsw[];

#define MEM_MAJOR       1

#endif /* CONF_H */