#
/*
 *	This file supports the ERRLOG system change
 *	see changes.h
 */
/*
 *	discerr.h
 *	header file for error logging procedure
 *	errors on the discs are stored in the unused area of the
 *	root super block - the structure below gives the
 *	positioning for the three devices at UKC
 *	To make it easier to find the root super block the
 *	file "alloc.c" has been modified (iinit()) to store
 *	the address of the super block in a variable superblk (defined
 *	in systm.h)
 */

#define NPRK	2	/* Number of physical RK drives */
#define NPRP	2	/* Number of physical RP drives */
#define NPRX	2	/* Number of physical RX drives */

struct disc_err
{	int	ds_pad[205];		/* Major info in super block */
	char	ds_mod;			/* super block modified flags */
	char	ds_ronly;
	long	ds_time;

	long	rk_time;		/* time of incident on rk */
	int 	rk_retry[NPRK];		/* Number of incidents on each drive */
	int	rk_stat[7];		/* Contents of status registers at start of last incident */

	long	rp_time;		/* Time of incident on rp */
	int	rp_retry[NPRP];		/* Number of incidents on rp */
	int	rp_stat[7];		/* Status registers at time of last incident */
	int	rp_suca;		/* suca register */

	long	rx_time;		/* time of incident on rx */
	int	rx_retry[NPRX];		/* Number of incidents on each drive */
	int	rx_edrive;		/* Drive number of incident */
	int	rx_esect;		/* sector in driver when incident took place */
	int	rx_etrk;		/* track */
	int	rx_estat;		/* status return from controller */
};
