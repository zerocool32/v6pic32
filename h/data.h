#
/*
 *	data.h
 *	This file contains all the data area definitions for the
 *	system
 *	it should be included in c.c
 */
#include "/usr/sys/param.h"
#include "/usr/sys/systm.h"
#include "/usr/sys/proc.h"
#include "/usr/sys/text.h"
#include "/usr/sys/inode.h"
#include "/usr/sys/buf.h"
#include "/usr/sys/file.h"

int	coremap[CMAPSIZ];	/* space for core allocation */
int	swapmap[SMAPSIZ];	/* space for swap allocation */
struct callo callout[NCALL];	/* callout structure */
struct mount mount[NMOUNT];	/* Mount structure */
struct proc proc[NPROC];	/* proc structure */
struct text text[NTEXT];	/* text structure */
struct inode inode [NINODE];	/* inode structure */
struct buf buf[NBUF];		/* buffer structure */
char buffers[NBUF][514];	/* The buffers proper */
struct file file[NFILE];	/* file structure */
