/*
 * dir.h
 * Copyright (c) 1992 Szigeti Szabolcs
 */

#ifndef DIR_H
#define DIR_H

struct dir
{
    ino_t dir_i;            /* inode of entry */
    char dir_nam[DIRSIZ];   /* name of entry */
};

#define L_DIR   (DIRSIZ+sizeof(ino_t))

#endif /* DIR_H */