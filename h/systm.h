#ifndef SYSTM_H
#define SYSTM_H

#include "types.h"

/*
 * Random set of variables
 * used by more than one
 * routine.
 */
#ifdef OLDTTYDRIVER
/*	Removed September 1977 due to new tty driver  */
char	canonb[CANBSIZ];	/* buffer for erase and kill (#@) */
#endif
word lock1(void);
void unlock(word);
int	*rootdir;		/* pointer to inode of root directory */
int	cputype;		/* type of cpu =40, 45, or 70 */
int	execnt;			/* number of processes in exec */
int	lbolt;			/* time of day in 60th not in time */
long	time;			/* time in sec from 1970 */
#ifndef NEW_SLEEP
long	tout;			/* time of day of next sleep */
#endif

#ifdef ACCT
long	etime;			/* elapsed time from system startup */
int 	*acctp;			/* inode of accounting file */
#endif

/*
 * The callout structure is for
 * a routine arranging
 * to be called by the clock interrupt
 * (clock.c) with a specified argument,
 * in a specified amount of time.
 * Used, for example, to time tab
 * delays on teletypes.
 */
struct	callo
{
	int	c_time;		/* incremental time */
	int	c_arg;		/* argument to routine */
	int	(*c_func)();	/* routine */
};

/*
 * Mount structure.
 * One allocated on every mount.
 * Used to find the super block.
 */
struct	mount
{
	int	m_dev;		/* device mounted */
	int	*m_bufp;	/* pointer to superblock */
	int	*m_inodp;	/* pointer to mounted on inode */
};

int	mpid;			/* generic for unique process id's */
char	runin;			/* scheduling flag */
char	runout;			/* scheduling flag */
char	runrun;			/* scheduling flag */
char	curpri;			/* more scheduling */
int	maxmem;			/* actual max memory per process */
int	*lks;			/* pointer to clock device */
int	rootdev;		/* dev of root see conf.c */
int	swapdev;		/* dev of swap see conf.c */
int	swplo;			/* block number of swap space */
int	nswap;			/* size of swap space */
int	updlock;		/* lock for sync */
int	*superblk;		/* Pointer to root super block used by ERRLOG - see rk.c rp.c etc */

void panic(char *message);

#endif /* SYSTM_H */