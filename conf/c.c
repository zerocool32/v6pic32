/*
 * c.c - Block/Character device table
 *
 * Copyright (c) 2023 Stefanos Stefanidis. All rights reserved.
 */

#include "../h/buf.h"
#include "../h/conf.h"
#include "../h/param.h"
#include "../h/types.h"

int nulldev();
int nodev();
int sdxsgtty(), sdxread(), sdxwrite();
int mmread(), mmwrite();
int	syopen(), syclose(), syread(), sywrite(), sysgtty();
int uartopen(), uartclose(), uartread(), uartwrite(), uartioctl();

int sdxopen(), sdxclose(), sdxstrategy();
extern struct buf sdxtab;

/* Block devices */
struct bdevsw bdevsw[] =
{
    sdxopen, sdxclose, sdxstrategy, &sdxtab, 	/* sdx = 0 */
    0
};

/* Character devices */
struct cdevsw cdevsw[] =
{
    uartopen, uartclose, uartread, uartwrite, uartioctl,    /* console = 0 */
    syopen, nulldev, syread, sywrite, sysgtty, nulldev,    /* tty = 1 */
    nulldev, nulldev, mmread, mmwrite, nodev,    /* mem = 2 */
    sdxopen, sdxclose, sdxread, sdxread, sdxwrite, sdxsgtty, /* rsdx = 3 */
    0
};

dev_t	rootdev	= makedev(0, 0);
dev_t	swapdev	= makedev(0, 1);