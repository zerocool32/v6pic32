/*
 * uproc.c
 * Copyright (c) 1992 Szigeti Szabolcs
 */

#include "../h/param.h"
#include "../h/types.h"
#include "../h/buf.h"
#include "../h/inode.h"
#include "../h/stat.h"
#include "../h/dir.h"

#ifndef KESH
void
proc3()
{
    sys_x(11,"/etc/init","\0\0\0",0,0);
	sys_x(1,1,0,0,0);
}
#endif

#ifdef KESH
void
proc3()
{
	int	fp;
	static char  c[30];
	static char *ap[5];
	char *cp,i;

	/*
	 * make console entry
	 *
	 * mknod /dev as directory
	 * link  /dev as /dev/.
	 * link  /.   as /dev/..
	 * mknod /dev/tty0 as char device
	*/

/*	sys_x(14,"/dev",0140755,0,0);
	sys_x(9,"/dev","/dev/.",0,0);
	sys_x(9,"/.","/dev/..",0,0);
	sys_x(14,"/dev/tty0",0120666,0,0);
	sys_x(36,0,0,0,0);
	sys_x(1,1,0,0,0);
*/

	fp=sys_x(5,"/dev/tty0",2,0,0);

	if (fp<0) sys_x(1,1,0,0,0); /* kill init */

	sys_x(4,fp,"UNIX V6\nPIC32 version\n\n",23,0);
l001:	sys_x(4,fp,"$ ",2,0,0);
	for(cp=c;cp<&c[30];*(cp++)=0);
	sys_x(3,fp,c,29,0);

	i=0;
	cp = &c[2];
	while (*cp==' '||*cp=='\t')
		cp++;
	ap[i++]=cp;
	for(;cp<&c[30];cp++)
		{
		if (*cp==0||*cp=='\r'||*cp=='\n')
			{
			*cp=0;
			break;
			}
		if (*cp==' '||*cp=='\t')
			{
			*(cp++)=0;
			while(*cp==' '||*cp=='\t')
				cp++;
			ap[i]=&c[2];
			ap[i++]=cp;
			}
		}

	switch(*(int*)c)
		{
	case 'ex':	{			/* Exec */
			sys_x(11,ap[0],"\0\0\0\0",0,0);
			break;
			}
	case 'gw':	{
			fprintf(fp,"sw:%u\n",sys_x(38,0,0,0,0));
			break;
			}
	case 'id':	{			/* Give ID	*/
			fprintf(fp,"UNIX V6\n");
			break;
			}
	case 'ge':	{
			get(fp,ap);
			break;
			}
	case 'sy':	{			/* Sync */
			sys_x(36,0,0,0,0);
			break;
			}
	case 'ls':	{			/* ls -l */ 
			lst(fp);
			break;
			}
	case 'cd':	{			/* chdir */
			cwd(fp,ap);
			break;
			}
	case 'mk':	{			/* make node */
			mkn(fp,ap);
			break;
			}
	case 'ln':	{			/* link file */
			lnk(fp,ap);
			break;
			}
	case 'rm':	{			/* unlink file	*/
			rmf(fp,ap);
			break;
			}
	case 'cm':	{			/* change mode	*/
			chm(fp,ap);
			break;
			}
	case 'co':	{           /* change owner	*/
			cho(fp,ap);
			break;
			}
	case 'cr':	{			/* create file	*/
			cre(fp,ap);
			break;
			}
	case 'cl':	{			/* close file 	*/
			clo(fp,ap);
			break;
			}
	case 'wr':	{			/* write file 	*/
			wri(fp,ap);
			break;
			}
	case 'op':	{
			ope(fp,ap);		/* open file	*/
			break;
			}
	case 're':	{  			/* read file	*/
			rea(fp,ap);
			break;
			}
	case 'ca':	{			/* cat file	*/
			cat(fp,ap);
			break;
			}
	case 'su':	{			/* set uid	*/
			sys_x(23,uatoi(ap[0],10),0,0,0);
			break;
			}
	case 'gu':	{			/* get uid	*/
			fprintf(fp,"%u\n",sys_x(24,0,0,0,0));
			break;
			}
	case 'ti':	{			/* set time	*/
			sys_x(25,uatoi(ap[0],10),0,0);
			break;
			}
	case 'gt':	{			/* get time	*/
			fprintf(fp,"%x%x\n",(long)sys_x(13,0,0,0,0));
			break;
			}
	case 'st':      {			/* stat */
			sta(fp,ap);
			break;
			}
	case 'mt':	{
			mou(fp,ap);
			break;
			}
	case 'um':	{
			umo(fp,ap);
			break;
			}
	case 'se':	{
			see(fp,ap);
			break;
			}

	default :	{
			fprintf(fp,"??syntax\n");
			}
		}
l002:	goto l001;

}

void
wri(int f, char *p[])
{
	char bu[20];
	int re;
	re=sys_x(3,f,bu,uatoi(p[1],10),0);
	fprintf (f,"%u\n",
	sys_x(4,uatoi(p[0],10),bu,re),0);
}

void
rea(int f, char *p[])
{
	char bu[16];
	int re;
	re=sys_x(3,uatoi(p[0],10),bu,16,0);
	fprintf (f,"%u\n",re);
	sys_x(4,f,bu,re,0);
}

void
see(int f, char *p[])
{
	fprintf (f,"%u\n",
		sys_x(19,uatoi(p[0],10),uatoi(p[1],10),uatoi(p[2],10),0));
}

void
ope(int f, char *p[])
{
	fprintf(f,"%u\n",sys_x(5,p[0],uatoi(p[1],10),0,0));
}

void
sta(int f, char *p[])
{
	struct stat fst;

	sys_x(18,p[0],&fst,0,0);
	fprintf (f,"dev/ino:%u-%u\nflg:0x%x\nnl:%u\n",fst.s_dev,fst.s_inumber,
		fst.s_flags,fst.s_nlinks);
	fprintf (f,"uid:%u/%u\n",fst.s_gid,fst.s_uid);
	fprintf (f,"AT:%x%x MT:%x%x\n",fst.s_actime,fst.s_modtime);
}

void
cre(int f, char *p[])
{
	int mod;
	mod = uatoi(p[1],8);
	mod=sys_x(8,p[0],mod,0,0);
	fprintf (f,"%u\n",mod);
}

void
clo(int f, char *p[])
{
	fprintf (f,"%u\n",
	sys_x(6,uatoi(p[0],10),0,0,0));
}

void
rmf(f,p)
{
	fprintf (f,"%u\n",
	sys_x(10,p[0],0,0,0));
}
mkn	(f,p)
int f;
char *p[];
{
	int mod,adr;
	mod = uatoi(p[1],8);
	adr = uatoi(p[2],10);
	fprintf (f,"%u\n",
	sys_x(14,p[0],mod,adr,0));
}
chm	(f,p)
int f;
char *p[];
{
	int mod;
	mod = uatoi(p[1],8);
	fprintf (f,"%u\n",
	sys_x(15,p[0],mod,0,0));
}


cho	(f,p)
int f;
char *p[];
{
	int own;
	own = uatoi(p[1],10);
	sys_x(16,p[0],own,0,0);
}


lnk	(f,p)
int f;
char *p[];
{
	fprintf (f,"%u\n",sys_x(9,p[0],p[1],0,0));
}


mou	(f,p)
int f;
char *p[];
{
	fprintf (f,"%u\n",sys_x(21,p[0],p[1],uatoi(p[2],10),0));
}

void
umo(int f, char *p[])
{
	fprintf (f,"%u\n",sys_x(22,p[0],0,0,0));
}

void
cwd(int f, char *p[])
{
	fprintf(f,"%u\n",sys_x(12,p[0],0,0,0));
}

/* list contents of directory */
void
lst(int f)
{
	int lfp;	/* file pointer . -ra	*/
	int i;
	static struct stat statb;
	static struct dir dirb;
	static char nb[15];

	if ((lfp=sys_x(5,".",0,0,0))==-1)
	{
		fprintf(f,"ls failed.\n");
		return;
	}
	while (sys_x(3,lfp,&dirb,L_DIR,0)==L_DIR)
	{
		if (dirb.dir_i)
		{
			for(i=0;i<14;i++)
			{
				if (dirb.dir_nam[i]==0)
				{
					nb[i+1]=0;
					break;
				}
				nb[i]=dirb.dir_nam[i];
			}
			sys_x(18,nb,&statb,0,0);
			switch(statb.s_flags&S_TYPE)
				{
				case S_PLAIN: i='-';
						break;
				case S_DIREC: i='d';
						break;
				case S_CHAR:  i='c';
						break;
				case S_BLOCK: i='b';
						break;
				}
			fprintf (f,"%c",i);
			fprintf (f,"%c%c%c",(statb.s_flags&0400)?'r':'-',
					    (statb.s_flags&0200)?'w':'-',
			(statb.s_flags&0100)?((statb.s_flags&04000)?'s':'x'):'-');
			fprintf (f,"%c%c%c",(statb.s_flags&0040)?'r':'-',
					    (statb.s_flags&0020)?'w':'-',
			(statb.s_flags&0010)?((statb.s_flags&02000)?'s':'x'):'-');
			fprintf (f,"%c%c%c ",(statb.s_flags&0004)?'r':'-',
					    (statb.s_flags&0002)?'w':'-',
					    (statb.s_flags&0001)?'x':'-');
		if ((statb.s_flags&S_TYPE)!=S_CHAR&&
		    (statb.s_flags&S_TYPE)!=S_BLOCK)
		    {
			fprintf (f,"%u %u/%u ",statb.s_nlinks,statb.s_gid,
				statb.s_uid);
			fprintf (f,"%u ",statb.s_size1);
		    }
		else
			fprintf (f,"%u %u/%u      ",statb.s_nlinks,major(statb.
				s_addr[0]),minor(statb.s_addr[0]));
			fprintf (f,"%s\n",nb);
			}
		for(i=0;i<14;nb[i]=dirb.dir_nam[i]=0)i++;

		}
	sys_x(6,lfp,0,0,0);
}

void
cat(int f, char *p[])
{
	char buf[16];
	int fi,re;
	if ((fi=sys_x(5,p[0],0,0,0))==-1)
		return;
	do
		{
		re=sys_x(3,fi,buf,16,0);
		sys_x(4,f,buf,re,0);
		}
	while (re==16);
	sys_x(6,fi,0,0,0);
}

void
get(int f, char *p[])
{
	int inf,of,cn;
	char buf;
	inf=sys_x(5,"/dev/sd1",0,0,0);	/* open */
	of=sys_x(8,p[0],0777,0,0);	/* creat*/
	sys_x(3,inf,&cn,2,0);		/* read */
	sys_x(19,inf,512,0,0);		/* seek */
	while (cn--)
		{
		sys_x(3,inf,&buf,1,0);	/* read */
		sys_x(4,of,&buf,1,0);	/* write*/
		}
	sys_x(6,inf,0,0,0);		/* close */
	sys_x(6,of,0,0,0);		/* close */
	sys_x(36,0,0,0,0);		/* sync  */
}

void
proc4()
{
	for(;;)
	{
		sys_x(35,30,0,0,0);
		sys_x(36,0,0,0,0);

	}
}

#endif	/* KESH */