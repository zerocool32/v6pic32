# v6pic32

A port of UNIX V6 to the PIC32 microcontroller.

# Terms and licensing

While most of the code is my own, portions are derived from other open-source projects:
* The UNIX V6 codebase is covered by the Caldera License Grant (see LICENSE.Caldera)
* The V6 kernel profiler's `printf` implementation is derived from [V7](https://github.com/calmsacibis995/v7-src/blob/master/usr/sys/sys/prf.c)
* The PIC32 emulator is derived from [RetroBSD](https://github.com/RetroBSD/retrobsd/tree/master/tools/virtualmips)

# Compilers
For building, V6/PIC32 uses GCC 4.8.1, which can be downloaded [here.](https://github.com/sergev/LiteBSD/releases/tag/tools)